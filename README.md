# ESGI - TP Test de performance
## Importer les fichiers csv en bdd
### Remplir les tables avec les csv
- dcexec php php artisan csvtodatabase [all]
Choix possibles (optionnel) :
+ registre_incidences, donnees_hospitalieres_classe_ages, donnees_hospitalieres, donnees_hospitalieres_etablissements, donnees_hospitalieres_nouveaux

## Importer un dump sql dans le container postgresql
- docker cp {PGSQL_FILE} {ID_CONTAINER}:/dbexport.sql
- docker-compose exec db psql -U admin laravel < /dbexport.sql

## Lancer k6
- docker run -i --net=host loadimpact/k6 run - < tests/script.js
