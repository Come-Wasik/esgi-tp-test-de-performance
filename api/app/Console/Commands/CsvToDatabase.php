<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DonneesHospitalieres;
use App\Models\DonneesHospitalieresClasseAge;
use App\Models\DonneesHospitalieresEtablissements;
use App\Models\DonneesHospitalieresNouveaux;
use App\Models\RegistreIncidence;
use App\Service\CsvToDbParser;
use App\Service\FileAnalyser;
use RuntimeException;

class CsvToDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csvtodatabase {table=all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse a csv file and fill the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var string $basePath The directory which contain the data file */
        $basePath = storage_path('app/bdd/data');

        switch ($this->argument('table')) {
            case 'all':
            case 'registre_incidences':
                $filepath = $basePath . '/covid-hospit-incid-reg-2021-04-15-19h15.csv';
                $instanceCount = FileAnalyser::getLineCount($filepath);
                /** @var Generator $generator A loopable element which parse a csv file entity by entity */
                $generator = CsvToDbParser::parseAndYield($filepath, RegistreIncidence::class);

                $this->comment('Start importing ' . $instanceCount . ' elements from "Registre Incidence"');
                $bar = $this->output->createProgressBar($instanceCount);
                $bar->start();
                foreach ($generator as $instance) {
                    $instance->save();
                    $bar->advance();
                }
                $bar->finish();
                $this->newLine();
                $this->info('Importation was a success');

                if ($this->argument('table') != 'all') {
                    break;
                }
            case 'donnees_hospitalieres_classe_ages':
                $filepath = $basePath . '/donnees-hospitalieres-classe-age-covid19-2021-04-15-19h15.csv';
                $instanceCount = FileAnalyser::getLineCount($filepath);
                /** @var Generator $generator A loopable element which parse a csv file entity by entity */
                $generator = CsvToDbParser::parseAndYield($filepath, DonneesHospitalieresClasseAge::class);

                $this->comment('Start importing ' . $instanceCount . ' elements from "Donnees Hospitalieres Classe Age"');
                $bar = $this->output->createProgressBar($instanceCount);
                $bar->start();
                foreach ($generator as $instance) {
                    $instance->save();
                    $bar->advance();
                }
                $bar->finish();
                $this->newLine();
                $this->info('Importation was a success');

                if ($this->argument('table') != 'all') {
                    break;
                }
            case 'donnees_hospitalieres':
                $filepath = $basePath . '/donnees-hospitalieres-covid19-2021-04-15-19h15.csv';
                $instanceCount = FileAnalyser::getLineCount($filepath);
                /** @var Generator $generator A loopable element which parse a csv file entity by entity */
                $generator = CsvToDbParser::parseAndYield($filepath, DonneesHospitalieres::class);

                $this->comment('Start importing ' . $instanceCount . ' elements from "Donnees Hospitalieres"');
                $bar = $this->output->createProgressBar($instanceCount);
                $bar->start();
                foreach ($generator as $instance) {
                    $instance->save();
                    $bar->advance();
                }
                $bar->finish();
                $this->newLine();
                $this->info('Importation was a success');

                if ($this->argument('table') != 'all') {
                    break;
                }
            case 'donnees_hospitalieres_etablissements':
                $filepath = $basePath . '/donnees-hospitalieres-etablissements-covid19-2021-04-15-19h15.csv';
                $instanceCount = FileAnalyser::getLineCount($filepath);
                /** @var Generator $generator A loopable element which parse a csv file entity by entity */
                $generator = CsvToDbParser::parseAndYield($filepath, DonneesHospitalieresEtablissements::class);

                $this->comment('Start importing ' . $instanceCount . ' elements from "Donnees Hospitalieres Etablissements"');
                $bar = $this->output->createProgressBar($instanceCount);
                $bar->start();
                foreach ($generator as $instance) {
                    $instance->save();
                    $bar->advance();
                }
                $bar->finish();
                $this->newLine();
                $this->info('Importation was a success');

                if ($this->argument('table') != 'all') {
                    break;
                }
            case 'donnees_hospitalieres_nouveaux':
                $filepath = $basePath . '/donnees-hospitalieres-nouveaux-covid19-2021-04-15-19h15.csv';
                $instanceCount = FileAnalyser::getLineCount($filepath);
                /** @var Generator $generator A loopable element which parse a csv file entity by entity */
                $generator = CsvToDbParser::parseAndYield($filepath, DonneesHospitalieresNouveaux::class);

                $this->comment('Start importing ' . $instanceCount . ' elements from "Donnees Hospitalieres Nouveaux"');
                $bar = $this->output->createProgressBar($instanceCount);
                $bar->start();
                foreach ($generator as $instance) {
                    $instance->save();
                    $bar->advance();
                }
                $bar->finish();
                $this->newLine();
                $this->info('Importation was a success');
                break;
            default:
                $this->error('Unknown table');
                return 1;
        }
        return 0;
    }
}
