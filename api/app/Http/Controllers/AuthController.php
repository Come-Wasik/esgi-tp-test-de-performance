<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class AuthController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => ['required', Password::min(8)]
        ]);

        // The validator failed ?
        if ($validator->fails()) {
            return response()->json([
                'message' => 'The submited form has error(s)',
                'errors' => $validator->errors()
            ], 400);
        }

        $user = User::create(
            $request->only('name', 'email', 'password')
        );

        $token = $user->createToken('perf-token')->plainTextToken;

        return response()->json([
            'user' => new UserResource($user),
            'token' => $token,
            'token_type' => 'bearer'
        ], 201);
    }

    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|max:255',
            'password' => 'required',
        ]);

        // The validator failed ?
        if ($validator->fails()) {
            return response()->json([
                'message' => 'The submited form has error(s)',
                'errors' => $validator->errors()
            ], 400);
        }

        // Try to login the user
        if (!auth()->attempt($request->only('email', 'password'))) {
            if (empty(User::whereEmail($request->get('email'))->count())) {
                return response()->json([
                    'message' => 'Email invalide'
                ], 401);
            }
            return response()->json([
                'message' => 'Email ou mot de passe invalide'
            ], 401);
        }

        // Créer un token nommé "perf-token" qui a les droits "doing:everything"
        $token = auth()->user()->createToken('perf-token', ['doing:everything']);

        // Renvoi le token
        return response()->json([
            'token' => $token->plainTextToken,
            'token_type' => 'bearer',
        ]);
    }

    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return UserResource|JsonResponse
     */
    public function profile(Request $request)
    {
        if (!auth()->user()->tokenCan('doing:everything')) {
            return \response()->json([
                'message' => 'No sufficient rights'
            ], 403);
        }
        return new UserResource($request->user());
    }

    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}
