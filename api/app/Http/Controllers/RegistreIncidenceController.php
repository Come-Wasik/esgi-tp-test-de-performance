<?php

namespace App\Http\Controllers;

use App\Http\Resources\RegistreIncidenceResource;
use App\Models\RegistreIncidence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class RegistreIncidenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = RegistreIncidence::paginate(12);
        $collection = RegistreIncidenceResource::collection($paginator);
        return response()->json([
            'collection'=> $collection,
            'onFirstPage' => $paginator->onFirstPage(),
            'hasMorePages' => $paginator->hasMorePages()
        ]);
    }

    /**
     * Display a very low and heavy version of the page (without paginator)
     */
    public function indexLow()
    {
        return response()->json([
            'collection' => RegistreIncidenceResource::collection(RegistreIncidence::all())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Set validation rules
        $validator = Validator::make($request->all(), [
            'jour' => 'required|date_format:Y-m-d',
            'nomReg' => 'required|string|max:50',
            'numReg' => 'required|integer',
            'incid_rea' => 'required|integer|min:0'
        ]);

        // The form failed ?
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        // Try to register data in model and model in database
        $model = new RegistreIncidence($validator->validated());
        $response = ($model)->save();

        // Testing database response
        if ($response === false) {
            return response()->json([
                'message' => 'The database had an error when inserting the data'
            ], 500);
        }

        // All works
        return [
            'idModel' => $model->id
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegistreIncidence  $registreIncidence
     * @return \Illuminate\Http\Response
     */
    public function show(RegistreIncidence $registreIncidence)
    {
        return new RegistreIncidenceResource($registreIncidence);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegistreIncidence  $registreIncidence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistreIncidence $registreIncidence)
    {
        // Set validation rules
        $validator = Validator::make($request->all(), [
            'jour' => 'required|date_format:Y-m-d',
            'nomReg' => 'required|string|max:50',
            'numReg' => 'required|integer',
            'incid_rea' => 'required|integer|min:0'
        ]);

        // The validator failed ?
        if ($validator->fails()) {
            return response()->json([
                'message' => 'The submited form has error(s)',
                'errors' => $validator->errors()
            ], 400);
        }

        // Try to register data in model and model in database
        $response = $registreIncidence->update($validator->validated());

        // Testing database response
        if ($response === false) {
            return response()->json([
                'message' => 'The database had an error when updating the data'
            ], 500);
        }

        // All works
        return [
            'state' => 'success',
            'messages' => [
                'updatedModel' => new RegistreIncidenceResource($registreIncidence)
            ]
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegistreIncidence  $registreIncidence
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistreIncidence $registreIncidence)
    {
        if (!auth()->check() || !Gate::allows('is_admin')) {
            return response()->json(['message' => 'Unauthorized'], 403);
        }

        $modelId = $registreIncidence->id;
        $response = $registreIncidence->delete();

        // Testing database response
        if ($response === false) {
            return response()->json([
                'message' => 'The database had an error when deleting the data'
            ], 500);
        }

        // All works
        return [
            'message' => "The $modelId model does not exists anymore"
        ];
    }
}
