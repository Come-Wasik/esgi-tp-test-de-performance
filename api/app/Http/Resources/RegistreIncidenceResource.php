<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RegistreIncidenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'jour' => $this->jour,
            'nomReg' => mb_convert_encoding($this->nomReg, 'utf-8', 'iso-8859-1'),
            'numReg' => $this->numReg,
            'incid_rea' => $this->incid_rea,
        ];
    }
}
