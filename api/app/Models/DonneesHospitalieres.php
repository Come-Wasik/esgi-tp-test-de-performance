<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonneesHospitalieres extends Model
{
    use HasFactory;

    protected $fillable = [
        'dep',
        'sexe',
        'jour',
        'hosp',
        'rea',
        'HospConv',
        'SSR_USLD',
        'autres',
        'rad',
        'dc'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
