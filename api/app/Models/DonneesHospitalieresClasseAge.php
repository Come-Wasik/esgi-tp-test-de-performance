<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonneesHospitalieresClasseAge extends Model
{
    use HasFactory;

    protected $fillable = [
        'reg',
        'cl_age90',
        'jour',
        'hosp',
        'rea',
        'HospConv',
        'SSR_USLD',
        'autres',
        'rad',
        'dc'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
