<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonneesHospitalieresEtablissements extends Model
{
    use HasFactory;

    protected $fillable = [
        'dep',
        'jour',
        'nb'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
