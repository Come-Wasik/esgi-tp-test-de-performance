<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonneesHospitalieresNouveaux extends Model
{
    use HasFactory;

    protected $fillable = [
        'dep',
        'jour',
        'incid_hosp',
        'incid_rea',
        'incid_dc',
        'incid_rad'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
