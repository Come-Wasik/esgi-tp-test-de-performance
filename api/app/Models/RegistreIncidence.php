<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistreIncidence extends Model
{
    use HasFactory;

    protected $fillable = [
        'jour',
        'nomReg',
        'numReg',
        'incid_rea'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
