<?php

namespace App\Service;

use Illuminate\Support\Facades\Storage;
use RuntimeException;

class CsvToDbParser
{
    /**
     * Parse each csv line to an entity model then yield it
     * 
     * @param string $filePath The path to a valid csv file
     * @param string $modelName The model parsed from the csv
     * 
     * @return $modelName[]
     */
    static public function parseAndYield(string $filePath, string $modelName)
    {
        $handle = fopen($filePath, 'r');
        if ($handle === false) {
            throw new RuntimeException('The file ' . $filePath . ' does not exists');
        }

        $fieldsNames = fgetcsv($handle, 0, ';', '"', "\n");
        if (empty($fieldsNames)) {
            throw new RuntimeException('The file content is empty');
        }

        while (($data = fgetcsv($handle, 0, ';', '"', "\n")) != false) {
            $constructArray = [];
            for ($i = 0; $i < count($data); $i++) {
                $constructArray[$fieldsNames[$i]] = ($data[$i] == 'NA' ? -1 : $data[$i]);
            }
            yield new $modelName($constructArray);
        }
        fclose($handle);
    }

    // static public function parse2(string $filePath, string $modelName)
    // {
    //     ini_set('memory_limit', '1024M');
    //     if (Storage::disk('bdd')->exists($filePath)) {
    //         $file = Storage::disk('bdd')->get($filePath);
    //     } else {
    //         throw new RuntimeException('The file ' . $filePath . ' does not exists');
    //     }

    //     $data = str_getcsv($file, "\n"); // Parse the rows
    //     if (empty($data)) {
    //         throw new RuntimeException('The file content is empty');
    //     }

    //     // Get and "pop" the fields name from the array
    //     $fieldsNamesRow = array_shift($data);
    //     $parsedFieldsNames = str_getcsv($fieldsNamesRow, ";");

    //     $counter = 0;
    //     $instanceCollection = [];
    //     // Register all row as an object collection
    //     foreach ($data as &$row) {
    //         // Parse the items in rows
    //         $row = str_getcsv($row, ";");
    //         // Create an instance with the row
    //         $constructArray = [];
    //         for ($i=0; $i < count($row); $i++) { 
    //             $constructArray[$parsedFieldsNames[$i]] = ($row[$i] == 'NA' ? -1 : $row[$i]);
    //         }
    //         $counter++;
    //         dump(new $modelName($constructArray));
    //         dump($counter);
    //         $instanceCollection[] =  new $modelName($constructArray);
    //     }

    //     return $instanceCollection;
    // }
}
