<?php

namespace App\Service;

use RuntimeException;

class FileAnalyser
{
    public static function getLineCount(string $filepath): int
    {
        if (!\file_exists($filepath)) {
            throw new RuntimeException('The file ' . $filepath . ' does not exists');
        }

        $shellOutput = shell_exec("wc -l $filepath | awk '{ print $1 }' ");
        if ($shellOutput === false) {
            throw new RuntimeException("The command \"wc -l $filepath | awk '{ print $1 }' \" has failed");
        }

        $lineCount = (int) trim($shellOutput);
        $lineCount--; // Get out the title line
        return $lineCount;
    }
}
