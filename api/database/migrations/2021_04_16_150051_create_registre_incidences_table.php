<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistreIncidencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registre_incidences', function (Blueprint $table) {
            $table->id();
            $table->timestamp('jour')->comment('Date de notification');
            $table->string('nomReg')->comment('Nom de la région');
            $table->integer('numReg')->comment('Numéro de la région');
            $table->integer('incid_rea')->comment('Nombre de nouveaux patients admis en réanimation dans les 24 dernières heures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registre_incidences');
    }
}
