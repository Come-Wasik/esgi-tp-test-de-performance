<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonneesHospitalieresClasseAgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donnees_hospitalieres_classe_ages', function (Blueprint $table) {
            $table->id();
            $table->integer('reg')->comment('Region');
            $table->integer('cl_age90')->comment('Classe age');
            $table->timestamp('jour')->comment('Date de notification');
            $table->integer('hosp')->comment('Nombre de personnes actuellement hospitalisées');
            $table->integer('rea')->comment('Nombre de personnes actuellement en réanimation ou soins intensifs');
            $table->integer('HospConv');
            $table->integer('SSR_USLD');
            $table->integer('autres');
            $table->integer('rad')->comment('Nombre cumulé de personnes retournées à domicile');
            $table->integer('dc')->comment('Nombre cumulé de personnes décédées;Total amout of deaths');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donnees_hospitalieres_classe_ages');
    }
}
