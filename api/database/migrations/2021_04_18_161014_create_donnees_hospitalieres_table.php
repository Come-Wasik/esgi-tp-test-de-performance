<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonneesHospitalieresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donnees_hospitalieres', function (Blueprint $table) {
            $table->id();
            $table->string('dep')->comment('Département');
            $table->integer('sexe')->comment('Sexe');
            $table->timestamp('jour')->comment('Date de notification');
            $table->integer('hosp')->comment('Nombre de personnes actuellement hospitalisées');
            $table->integer('rea')->comment('Nombre de personnes actuellement en réanimation ou soins intensifs');
            $table->integer('HospConv');
            $table->integer('SSR_USLD');
            $table->integer('autres');
            $table->integer('rad')->comment('Nombre cumulé de personnes retournées à domicile');
            $table->integer('dc')->comment('Nombre cumulé de personnes décédées;Total amout of deaths');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donnees_hospitalieres');
    }
}
