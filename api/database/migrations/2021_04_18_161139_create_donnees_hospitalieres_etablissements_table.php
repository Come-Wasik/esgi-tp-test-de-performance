<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonneesHospitalieresEtablissementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donnees_hospitalieres_etablissements', function (Blueprint $table) {
            $table->id();
            $table->string('dep')->comment('Département');
            $table->timestamp('jour')->comment('Date de notification');
            $table->integer('nb')->comment('Nombre cumulé de services hospitaliers ayant déclaré au moins un cas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donnees_hospitalieres_etablissements');
    }
}
