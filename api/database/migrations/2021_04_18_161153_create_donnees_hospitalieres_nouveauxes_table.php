<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonneesHospitalieresNouveauxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donnees_hospitalieres_nouveauxes', function (Blueprint $table) {
            $table->id();
            $table->string('dep')->comment('Département');
            $table->timestamp('jour');
            $table->integer('incid_hosp')->comment('Nombre quotidien de personnes nouvellement hospitalisées');
            $table->integer('incid_rea')->comment('Nombre quotidien de nouvelles admissions en réanimation');
            $table->integer('incid_dc')->comment('Nombre quotidien de personnes nouvellement décédées');
            $table->integer('incid_rad')->comment('Nombre quotidien de nouveaux retours à domicile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donnees_hospitalieres_nouveauxes');
    }
}
