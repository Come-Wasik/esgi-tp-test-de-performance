<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Votre mot de passe a été réinitialisé !',
    'sent' => 'Nous nous avons envoyé un mail pour réinitialiser votre mot de passe !',
    'throttled' => 'Veuillez attendre avant de réésayer.',
    'token' => 'Ce token de réinitialisation de mot de passe est invalide. Avez-vous correctement suivi le lien de réinitialisation? Peut-être devrez vous renvoyer un mail pour changer votre mot de passe.',
    'user' => "Nous ne trouvons aucun utilisateur avec cette adresse email.",

];
