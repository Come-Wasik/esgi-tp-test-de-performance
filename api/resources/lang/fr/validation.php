<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Le champ :attribute doit être accepté.',
    'active_url' => 'Le champ :attribute ne renseigne pas un une URL valide.',
    'after' => 'Le champ :attribute doit être une date après :date.',
    'after_or_equal' => 'Le champ :attribute doit êre une date égale ou suivant le :date.',
    'alpha' => 'Le champ :attribute ne peut contenir que des lettres.',
    'alpha_dash' => 'Le champ :attribute ne peut contenir que des lettres, chiffres, tirets et underscores.',
    'alpha_num' => 'Le champ :attribute ne peut contenir que des lettres et des chiffres.',
    'array' => 'Le champ :attribute doit être un tableau.',
    'before' => 'Le champ :attribute doit être une date précédant :date.',
    'before_or_equal' => 'Le champ :attribute doit être une date précédant ou égale à :date.',
    'between' => [
        'numeric' => 'Le champ :attribute doit contenir un nombre entre :min et :max.',
        'file' => 'Le champ :attribute doit faire une taille entre :min et :max kilobits.',
        'string' => 'Le champ :attribute doit contenir entre :min et :max caractères.',
        'array' => 'Le champ :attribute doit contenir entre :min et :max éléments.',
    ],
    'boolean' => 'Le champ :attribute doit valoir vrai ou faux.',
    'confirmed' => 'La confirmation de :attribute ne correspond pas.',
    'date' => 'Le champ :attribute n\'est pas une date valide.',
    'date_equals' => 'Le champ :attribute doit être une date égale à :date.',
    'date_format' => 'Le champ :attribute ne correspond pas avec le format :format.',
    'different' => 'Le champ :attribute et :other doivent être différents.',
    'digits' => 'Le champ :attribute doit comporter :digits chiffres.',
    'digits_between' => 'Le champ :attribute doit comporter entre :min et :max chiffres.',
    'dimensions' => 'Le champ :attribute a des dimensions d\'image non valides.',
    'distinct' => 'Le champ :attribute a une valeur en double.',
    'email' => 'Le champ :attribute doit être un email valide (e.g. some@thing.fr).',
    'ends_with' => 'Le champ :attribute doit terminer avec l\'une des valeurs suivantes : :values.',
    'exists' => 'Le champ sélectionné :attribute est invalide.',
    'file' => 'Le champ :attribute doit être un fichier.',
    'filled' => 'Le champ :attribute ne peut pas être vide.',
    'gt' => [
        'numeric' => 'Le champ :attribute doit avoir un nombre plus élevé que :value.',
        'file' => 'Le champ :attribute doit avoir une taille plus élevée que :value kilobits.',
        'string' => 'Le champ :attribute doit contenir plus que :value caractères.',
        'array' => 'Le champ :attribute doit contenir plus de :value éléments.',
    ],
    'gte' => [
        'numeric' => 'Le champ :attribute doit avoir un nombre égal ou plus élevé que :value.',
        'file' => 'Le champ :attribute doit avoir une taille minimale de :value kilobits.',
        'string' => 'Le champ :attribute doit contenir au moins :value caractères.',
        'array' => 'Le champ :attribute doit contenir au moins :value éléments.',
    ],
    'image' => 'Le champ :attribute doit être une image.',
    'in' => 'Le champ sélectionné :attribute est invalide.',
    'in_array' => 'Le champ :attribute n\'existe pas dans :other.',
    'integer' => 'Le champ :attribute doit être un nombre.',
    'ip' => 'Le champ :attribute doit être une adresse IP valide.',
    'ipv4' => 'Le champ :attribute doit être une adresse IPv4 valide.',
    'ipv6' => 'Le champ :attribute doit être une adresse IPv6 valide.',
    'json' => 'Le champ :attribute doit être du JSON valide.',
    'lt' => [
        'numeric' => 'Le champ :attribute doit comporter un nombre moins élevé que :value.',
        'file' => 'Le champ :attribute doit avoir une taille moins élevée que :value kilobits.',
        'string' => 'Le champ :attribute doit contenir moins de :value caractères.',
        'array' => 'Le champ :attribute doit contenir moins de :value éléments.',
    ],
    'lte' => [
        'numeric' => 'Le champ :attribute doit comporter un nombre égal ou moins de :value.',
        'file' => 'Le champ :attribute doit avoir une taille maximale de :value kilobits.',
        'string' => 'Le champ :attribute doit contenir maximum :value caractères.',
        'array' => 'Le champ :attribute doit contenir moins de :value éléments.',
    ],
    'max' => [
        'numeric' => 'Le champ :attribute doit comporter un nombre valant maximum :max.',
        'file' => 'Le champ :attribute doit avoir une taille maximale de :max kilobits.',
        'string' => 'Le champ :attribute doit contenir maximum :max caractères.',
        'array' => 'Le champ :attribute doit contenir maximum :max éléments.',
    ],
    'mimes' => 'Le fichier :attribute doit contenir les types suivants : :values.',
    'mimetypes' => 'Le fichier :attribute doit contenir les types suivants : :values.',
    'min' => [
        'numeric' => 'Le champ :attribute doit comporter un nombre valant minimum :min.',
        'file' => 'Le champ :attribute doit avoir une taille minimale de :min kilobits.',
        'string' => 'Le champ :attribute doit contenir au minimum :min caractères.',
        'array' => 'Le champ :attribute doit contenir au moins :min items.',
    ],
    'multiple_of' => 'Le champ :attribute doit être un multiple de :value.',
    'not_in' => 'La valeur sélectionnée du champ :attribute n\'est pas valide.',
    'not_regex' => 'Le format du champ :attribute n\'est pas valide.',
    'numeric' => 'Le champ :attribute doit être un nombre.',
    'password' => 'Le mot de passe est incorrect.',
    'present' => 'Le champ :attribute doit être présent.',
    'regex' => 'Le format du champ :attribute n\'est pas valide.',
    'required' => 'Le champ :attribute est manquant et requis.',
    'required_if' => 'Le champ :attribute est requis quand :other vaut :value.',
    'required_unless' => 'Le champ :attribute est requis sauf si :other vaut :values.',
    'required_with' => 'Le champ :attribute est requis quand :values est présent.',
    'required_with_all' => 'Le champ :attribute est requis quand :values sont présent.',
    'required_without' => 'Le champ :attribute est requis quand :values n\'est pas présent.',
    'required_without_all' => 'Le champ :attribute est requis quand aucune de ces valeurs ne sont présentes : :values.',
    'same' => 'Les champs :attribute et :other doivent correspondre.',
    'size' => [
        'numeric' => 'Le champ :attribute doit valoir :size.',
        'file' => 'Le champ :attribute doit avoir une taille de :size kilobits.',
        'string' => 'Le champ :attribute doit contenir :size caractères.',
        'array' => 'Le champ :attribute doit contenir :size éléments.',
    ],
    'starts_with' => 'Le champ :attribute doit commencer par une des valeurs suivantes : :values.',
    'string' => 'Le champ :attribute doit être du texte.',
    'timezone' => 'Le champ :attribute doit correspondre à une zone valide.',
    'unique' => 'La valeur du champ :attribute a déjà été prise.',
    'uploaded' => 'Le champ :attribute a échoué lors de son envoi.',
    'url' => 'Le format de :attribute est invalide.',
    'uuid' => 'Le champ :attribute doit être un UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
