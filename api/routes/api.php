<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistreIncidenceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::match(['GET', 'POST'], 'logout', [AuthController::class, 'logout'])
        ->middleware('auth:sanctum');
        
    Route::get('must-be-authenticated', function() {
        return response()->json(['message' => 'Unauthenticated'], 403);
    })->name('must_be_authenticated');
    
    Route::get('profile', [AuthController::class, 'profile'])
        ->middleware('auth:sanctum');
});

Route::prefix('registre-incidence')->name('registre-incidence.')->group(function() {
    Route::get('/', [RegistreIncidenceController::class, 'index']);

    Route::get('/index-low', [RegistreIncidenceController::class, 'indexLow']);

    Route::post('/', [RegistreIncidenceController::class, 'store'])
        ->middleware('auth:sanctum');

    Route::get('{registre_incidence}', [RegistreIncidenceController::class, 'show']);

    Route::match(['PUT', 'PATCH'],'/{registre_incidence}/edit', [RegistreIncidenceController::class, 'update'])
        ->middleware('auth:sanctum');

    Route::delete('{registre_incidence}', [RegistreIncidenceController::class, 'destroy'])
        ->middleware('auth:sanctum');
});