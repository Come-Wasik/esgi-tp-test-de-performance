<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        // On tente de se connecter avec ces identifiants via l'api
        $response = Http::post('http://nginx:80/api/auth/login', $credentials);
        if ($response->failed()) {
            return redirect(URL::previous())->withErrors(['error' => $response->json()['message']]);
        }

        // L'autentification a réussi
        // Mais juste avant, on vérifie que l'on prend bien en charge le type du token reçu
        $token = $response->json()['token'];
        $tokenType = $response->json()['token_type'];

        if ($tokenType != 'bearer') {
            abort('501', 'Only accept bearer token');
        }

        // // On requête les informations du profil utilisateur
        // $response = Http::withToken($token)->get('http://nginx:80/api/auth/profile');
        // if ($response->failed()) {
        //     return redirect(URL::previous())->withErrors(['error' => $response->json()['message']]);
        // }

        // // On a en notre possession toutes les données pour créer et loger un utilisateur !
        // $userMeta = $response->json()['data'];
        // $user = new User([
        //     'name' => $userMeta['name'],
        //     'email' => $userMeta['email'],
        //     'token' => $token,
        //     'token_type' => $tokenType,
        //     'created_at' => new \Datetime($userMeta['created_at'])
        // ]);

        // Log l'utilisateur dans l'application laravel
        // $userId = optional(User::where(['email' => $user->email])->first())->id;
        // if ($userId === null) {
        //     $user->save();
        // } else {
        //     $user->id = $userId;
        //     $user->update();
        // }

        // Auth::login($user);
        $request->session()->put('auth-token', $token);
        $request->session()->put('auth-token-type', $tokenType);
        // $request->session()->regenerate();
        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        // Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('home');
    }
}
