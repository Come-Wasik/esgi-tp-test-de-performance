<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $credentials = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        // On tente de se connecter avec ces identifiants via l'api
        $response = Http::post('http://nginx:80/api/auth/register', $credentials);
        if ($response->failed()) {
            return redirect(URL::previous())->withErrors(['error' => $response->json()['message']]);
        }

        $token = $response->json()['token'];
        $tokenType = $response->json()['token_type'];

        if ($tokenType != 'bearer') {
            abort('501', 'Only accept bearer token');
        }

        // event(new Registered($user));
        // Auth::login($user);
        $request->session()->put('auth-token', $token);
        $request->session()->put('auth-token-type', $tokenType);

        return redirect()->intended(RouteServiceProvider::HOME);
    }
}
