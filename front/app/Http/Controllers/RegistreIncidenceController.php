<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RegistreIncidenceController extends Controller
{
    public function __construct() {
        $this->middleware('has-auth-token', ['only' => ['create', 'edit', 'delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reg-incid.index');
    }

    public function indexLow()
    {
        $response = Http::get('http://nginx:80/api/registre-incidence/index-low');
        return view('reg-incid.index-low', [
            'data' => $response->json()['collection']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reg-incid.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('reg-incid.show', [
            'indice' => $id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('reg-incid.edit', [
            'indice' => $id
        ]);
    }
}
