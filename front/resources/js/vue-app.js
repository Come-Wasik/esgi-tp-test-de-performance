// Import vuejs and its router
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Define route components.
// These can be imported from other files
// Main pages
import App from './views/app.vue'
const Bar = { template: '<div>bar</div>' }
const User = { template: '<div>User {{ $route.params.id }}</div>' }
const Article = { props: ['id'], template: '<div> Article {{ id }}</div>' }
const Error404 = { template: '<div>Aucune page trouvée</div>' }


// Create the router instance and pass the `routes` option
const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: App, name: 'home' },
        { path: '/bar', component: Bar },
        { path: '/user/:id', component: User, name: 'user' },
        { path: '/article/:id', component: Article, props: true, name: 'article'},
        { path: '*', component: Error404}
    ]
})

// Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
  router
}).$mount('#app')
