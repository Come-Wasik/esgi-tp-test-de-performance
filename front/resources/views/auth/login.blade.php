@extends('layout')

@section('body')
    <main class="mt-16 mx-auto lg:w-1/3 md:1/2 w-full">
        <div class="w-full max-w-md">
            <form class="bg-white shadow-lg rounded px-12 pt-6 pb-8 mb-4" name="connexionForm"
                action="{{ route('auth.store') }}" method="POST">
                <h1 class="text-gray-800 text-2xl flex justify-center border-b-2 py-2 mb-4">
                    Formulaire de connexion
                </h1>
                @csrf
                <section class="mb-4">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="email">
                        Email
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        name="email" id="email" v-model="form.email" type="email" required="" autofocus=""
                        placeholder="Email">
                    @error('email')
                        <div class="text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </section>
                <section class="mb-6">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="password">
                        Mot de passe
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        v-model="form.password" id="password" type="password" placeholder="Password" name="password"
                        required="" autocomplete="current-password">
                    @error('password')
                        <div class="text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </section>

                @error('error')
                    <div class="text-red-500 text-sm">{{ $message }}</div>
                @enderror
                <section class="flex items-center justify-between">
                    <button
                        class="px-4 py-2 rounded text-white inline-block shadow-lg bg-blue-500 hover:bg-blue-600 focus:bg-blue-700"
                        type="submit">Se connecter</button>
                </section>
            </form>
        </div>
    </main>
@endsection
