@extends('layout')

@section('body')
    <main class="mt-16 mx-auto lg:w-1/3 md:1/2 w-full">
        <div class="w-full max-w-md">
            <p id="user-message" class="text-gray-500 bold text-center"></p>
        </div>
    </main>
@endsection

@section('body-javascript')
    <script>
        // On va récupérer la balise html contenant le token de connexion
        let authTokenTag = document.querySelector('meta[name="auth-token"]');

        // A la moindre erreur, déconnecter l'utilisateur
        function toDoIfError() {
            // document.location.href = '{{ route('auth.destroy') }}';
        }
        
        // Seul les tokens bearer sont pris en charge
        if (authTokenTag.getAttribute('type') === 'bearer' && authTokenTag.content.length > 0) {
            axios.get("http://localhost:8080/api/auth/profile", {
                headers: {
                    authorization: `Bearer ${authTokenTag.content}`
                }
            }).then((response) => {
                let user = response.data.data;
                document.querySelector('#user-message').textContent = `Bienvenue ${user.name} (${user.email})`;
            }).catch((err) => {
                toDoIfError();
            });
        } else {
            // L'utilisateur n'est pas connecté
            toDoIfError();
        }

    </script>
@endsection
