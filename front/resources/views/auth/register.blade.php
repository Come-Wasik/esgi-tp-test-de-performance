@extends('layout')

@section('body')
    <main class="mt-16 mx-auto lg:w-1/3 md:1/2 w-full">
        <div class="w-full max-w-md">
            <form class="bg-white shadow-lg rounded px-12 pt-6 pb-8 mb-4" name="connexionForm"
                action="{{ route('auth.register.store') }}" method="POST">
                @csrf
                <div class="text-gray-800 text-2xl flex justify-center border-b-2 py-2 mb-4">
                    Formulaire d'inscription
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="name">
                        Nom d'utilisateur
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="name" name="name" v-model="form.name" type="text" required="" autofocus=""
                        placeholder="Nom d'utilisateur" value="{{ old('name') }}">
                    @error('name')
                        <div class="text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="email">
                        Email
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="email" name="email" v-model="form.email" type="email" required="" autofocus=""
                        placeholder="Email" value="{{ old('email') }}">
                    @error('email')
                        <div class="text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-2">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="password">
                        Mot de passe
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        id="password" v-model="form.password" type="password" placeholder="Password" name="password"
                        required="" autocomplete="current-password">
                    @error('password')
                        <div class="text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="password">
                        Répétez le mot de passe
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        id="password_confirmation" v-model="form.password_confirmation" type="password"
                        name="password_confirmation" required="" autocomplete="current-password">
                    @error('password')
                        <div class="text-red-500 text-sm">{{ $message }}</div>
                    @enderror
                </div>

                @error('error')
                    <div class="text-red-500 text-sm">{{ $message }}</div>
                @enderror
                
                <div class="flex items-center justify-between">
                    <button
                        class="px-4 py-2 rounded text-white inline-block shadow-lg bg-blue-500 hover:bg-blue-600 focus:bg-blue-700"
                        type="submit">S'inscrire</button>
                </div>

                <p class="mt-2 text-green-500 text-center" id="success-text"></p>
                <p class="mt-2 text-red-500 text-center" id="error-text"></p>
            </form>
        </div>
    </main>
@endsection
