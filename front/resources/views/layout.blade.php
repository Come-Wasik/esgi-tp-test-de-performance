<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth-token" content="{{ session('auth-token') }}" type="{{ session('auth-token-type') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body>
    <nav class="mt-6 mx-4">
        <a href="{{ route('home') }}" class="text-blue-800 border border-blue-300 p-1 rounded">Home</a>
        @if (session()->has('auth-token'))
            <a href="{{ route('auth.profile') }}" class="text-blue-800 border border-blue-300 p-1 rounded">Profil
                utilisateur</a>
            <a href="{{ route('auth.destroy') }}"
                class="text-blue-800 border border-blue-300 p-1 rounded">Déconnexion</a>
        @else
            <a href="{{ route('auth.register') }}"
                class="text-blue-800 border border-blue-300 p-1 rounded">S'inscrire</a>
            <a href="{{ route('auth.create') }}" class="text-blue-800 border border-blue-300 p-1 rounded">Se
                connecter</a>
        @endauth
        <a href="{{ route('registre-incidence.index') }}"
            class="text-blue-800 border border-blue-300 p-1 rounded">Registre incidence</a>
        <a href="{{ route('registre-incidence.index-low') }}"
            class="text-blue-800 border border-blue-300 p-1 rounded">Registre incidence (version lourde)</a>
</nav>

<main class="mx-8 mt-4">
    @yield('body')
</main>

<script src="{{ mix('js/app.js') }}"></script>
@yield('body-javascript')
</body>

</html>
