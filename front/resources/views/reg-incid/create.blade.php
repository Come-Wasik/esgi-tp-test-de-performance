@extends('layout')

@section('body')
    <main class="mt-16 mx-auto lg:w-1/3 md:1/2 w-full">
        <div class="w-full max-w-md">
            <form class="bg-white shadow-lg rounded px-12 pt-6 pb-8 mb-4" name="regIncidForm" onsubmit="submitForm()">
                <!-- @csrf -->
                <div class="text-gray-800 text-2xl flex justify-center border-b-2 py-2 mb-4">
                    Formulaire d'ajout dans le registre d'incidence
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="date">
                        Date
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        name="date" v-model="form.date" type="date" required="" autofocus="">
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="nomRegion">
                        Nom de la région
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        v-model="form.nomRegion" type="nomRegion" name="nomRegion" required="">
                </div>
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="numRegion">
                        Numéro de la région
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        v-model="form.numRegion" type="numRegion" name="numRegion" required="">
                </div>
                <div class="mb-6">
                    <label class="block text-gray-700 text-sm font-normal mb-2" for="countRea">
                        Nombre de réanimations
                    </label>
                    <input
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        v-model="form.countRea" type="numRegion" name="countRea" required="">
                </div>
                <div class="flex items-center justify-between">
                    <button
                        class="px-4 py-2 rounded text-white inline-block shadow-lg bg-blue-500 hover:bg-blue-600 focus:bg-blue-700"
                        type="submit">Ajouter cet élément</button>
                </div>

                <p class="mt-2 text-green-500 text-center" id="success-text"></p>
                <p class="mt-2 text-red-500 text-center" id="error-text"></p>
            </form>
        </div>
    </main>
@endsection

@section('body-javascript')
    <script>
        function submitForm() {
            event.preventDefault();
            const authTokenTag = document.querySelector('meta[name="auth-token"]');
            const textError = document.querySelector('#error-text');
            const textSuccess = document.querySelector("#success-text");

            if (authTokenTag.getAttribute('type') !== 'bearer' || !authTokenTag.content) {
                axios.post("http://localhost:8080/api/registre-incidence", {
                    "jour": document.forms['regIncidForm'].elements['date'].value,
                    "nomReg": document.forms['regIncidForm'].elements['nomRegion'].value,
                    "numReg": document.forms['regIncidForm'].elements['numRegion'].value,
                    "incid_rea": document.forms['regIncidForm'].elements['countRea'].value
                }, {
                    headers: {
                        authorization: `Bearer ${authTokenTag.content}`
                    }
                }).then((response) => {
                    textSuccess.textContent =
                        "Element enregistré à l'identifiant " + response.data.idModel;

                }).catch((err) => {
                    if (err.response) {
                        switch (err.response.status) {
                            case 400:
                                // Erreur dans le formulaire
                                let errors = "";
                                for (const errorField in err.response.data.errors) {
                                    if (Object.hasOwnProperty.call(err.response.data.errors, errorField)) {
                                        const errorMessage = err.response.data.errors[errorField];
                                        errors += errorMessage + " ";
                                    }
                                }
                                textError.textContent =
                                    "Le formulaire envoyé a rencontré une/des erreur(s) : " + errors;

                                break;
                            case 401:
                                textError.textContent = "Utilisateur non connecté";

                                break;
                            case 403:
                                textError.textContent =
                                    "Vous n'avez pas les droits nécéssaire pour ajouter un élément";
                                break;
                            default:
                                textError.textContent = "Erreur inconnue";
                                break;
                        }
                    } else {
                        // Erreur inconnue
                        textError.textContent = "Erreur inconnue";
                    }
                });
            } else {
                document.querySelector("#error-text").textContent = "L'utilisateur n'est pas connecté";
            }
        }

    </script>
@endsection
