@extends('layout')

@section('body')
    {{-- Utilisé pour afficher certains messages de succès --}}
    <div class="hidden mb-8 block text-sm text-left text-green-600 bg-green-200 border border-green-400 h-12 flex items-center p-4 rounded-sm"
        id="success-text" role="alert">
        Succès...
    </div>
    {{-- Utilisé pour afficher certains messages d'erreur --}}
    <div class="hidden mb-8 block text-sm text-left text-red-600 bg-red-200 border border-red-400 h-12 flex items-center p-4 rounded-sm"
        id="error-text" role="alert">
        Erreur...
    </div>
    <a href="{{ route('registre-incidence.create') }}"
        class="my-4 px-4 py-3 bg-green-600 rounded-md text-white font-medium tracking-wide hover:bg-green-500 ml-3 inline-block font-bold">
        Ajouter un élément</a>

    <table class="min-w-full table-auto" id="main-table">
        <thead class="justify-between">
            <tr class="bg-gray-800">
                <th class="px-16 py-2">
                    <span class="text-gray-300">Id</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Date</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Région</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Nombre d'admis en réanimation</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Action</span>
                </th>
            </tr>
        </thead>
        <tbody class="bg-gray-200">
            @foreach ($data as $item)
                <tr>
                    <td class="px-16 py-2 text-center">{{ $item['id'] }}</td>
                    <td class="px-16 py-2 text-center">{{ $item['jour'] }}</td>
                    <td class="px-16 py-2 text-center">{{ $item['nomReg'] }} ({{ $item['numReg'] }})</td>
                    <td class="px-16 py-2 text-center">{{ $item['incid_rea'] }}</td>
                    <td class="px-16 py-2 text-center">
                        @if(session()->has('auth-token'))
                        <button onclick="window.location.href = `${window.location.origin}/registre-incidence/{{ $item['id'] }}/edit`"
                            class="text-white hover:text-indigo-900 bg-blue-600 hover:bg-blue-500 p-2 mr-2 rounded-md">Edit</button>
                        <button class="text-white hover:text-red-900 bg-red-600 hover:bg-red-500 p-2 mr-2 rounded-md"
                            onclick="deleteComment({{ $item['id'] }})">Delete</button>
                        @else
                        Veuillez vous connecter
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('body-javascript')
    <script>
        function deleteComment(number) {
            if (confirm('Souhaitez-vous réellement supprimer cette donnée') == false) {
                return;
            }

            const authTokenTag = document.querySelector('meta[name="auth-token"]');
            const textError = document.querySelector('#error-text');
            const textSuccess = document.querySelector("#success-text");

            if (authTokenTag.getAttribute('type') === 'bearer' || authTokenTag.content.length >
                0) {
                axios.delete(`http://localhost:8080/api/registre-incidence/${number}`, {
                        headers: {
                            authorization: `Bearer ${authTokenTag.content}`
                        }
                    })
                    .then(response => {
                        // On cache une possible erreur affichée
                        textError.textContent = "";
                        textError.classList.add("hidden");

                        // On affiche un message de succès
                        textSuccess.classList.remove("hidden");
                        textSuccess.textContent =
                            "L'élément a été supprimé avec succès. Veuillez recharger la page.";
                    })
                    .catch(err => {
                        // On va afficher une erreur (on cache le message de succès au cas où il serait affiché)
                        textError.classList.remove("hidden");
                        textSuccess.classList.add("hidden");

                        if (err.response) {
                            switch (err.response.status) {
                                case 401:
                                    textError.textContent = "Utilisateur non connecté";
                                    break;
                                case 403:
                                    textError.textContent =
                                        "Vous n'avez pas les droits nécessaires";
                                    break;
                                case 404:
                                    textError.textContent =
                                        "L'élément n'existe pas ou plus";
                                    break;
                                case 500:
                                default:
                                    textError.textContent = "Erreur inconnue";
                                    break;
                            }
                        } else {
                            // Erreur inconnue
                            textError.textContent = "Erreur inconnue";
                        }
                    });
            }
        };

    </script>
@endsection
