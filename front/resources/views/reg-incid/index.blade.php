@extends('layout')

@section('body')
    {{-- Utilisé pour afficher certains messages de succès --}}
    <div class="hidden mb-8 block text-sm text-left text-green-600 bg-green-200 border border-green-400 h-12 flex items-center p-4 rounded-sm"
        id="success-text" role="alert">
        Succès...
    </div>
    {{-- Utilisé pour afficher certains messages d'erreur --}}
    <div class="hidden mb-8 block text-sm text-left text-red-600 bg-red-200 border border-red-400 h-12 flex items-center p-4 rounded-sm"
        id="error-text" role="alert">
        Erreur...
    </div>
    <a href="{{ route('registre-incidence.create') }}"
        class="my-4 px-4 py-3 bg-green-600 rounded-md text-white font-medium tracking-wide hover:bg-green-500 ml-3 inline-block font-bold">
        Ajouter un élément</a>

    <table class="min-w-full table-auto" id="main-table">
        <thead class="justify-between">
            <tr class="bg-gray-800">
                <th class="px-16 py-2">
                    <span class="text-gray-300">Id</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Date</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Région</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Nombre d'admis en réanimation</span>
                </th>
                <th class="px-16 py-2">
                    <span class="text-gray-300">Action</span>
                </th>
            </tr>
        </thead>
        <tbody class="bg-gray-200">

        </tbody>
    </table>

    <div class="mt-4 flex justify-center">
        <button id="previous_button"
            class="border border-blue-500 text-blue-500 block rounded-sm font-bold py-4 px-6 mr-2 flex items-center hover:bg-blue-500 hover:text-white rounded disabled:opacity-50">
            <svg class="h-5 w-5 mr-2 fill-current" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-49 141 512 512"
                style="enable-background:new -49 141 512 512;" xml:space="preserve">
                <path id="XMLID_10_"
                    d="M438,372H36.355l72.822-72.822c9.763-9.763,9.763-25.592,0-35.355c-9.763-9.764-25.593-9.762-35.355,0 l-115.5,115.5C-46.366,384.01-49,390.369-49,397s2.634,12.989,7.322,17.678l115.5,115.5c9.763,9.762,25.593,9.763,35.355,0 c9.763-9.763,9.763-25.592,0-35.355L36.355,422H438c13.808,0,25-11.193,25-25S451.808,372,438,372z">
                </path>
            </svg>
            Page précédente
        </button>
        <button id="next_button"
            class="border border-blue-500 bg-blue-500 text-white block rounded-sm font-bold py-4 px-6 ml-2 flex items-center rounded disabled:opacity-50">
            Page suivante
            <svg class="h-5 w-5 ml-2 fill-current" clasversion="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-49 141 512 512"
                style="enable-background:new -49 141 512 512;" xml:space="preserve">
                <path id="XMLID_11_"
                    d="M-24,422h401.645l-72.822,72.822c-9.763,9.763-9.763,25.592,0,35.355c9.763,9.764,25.593,9.762,35.355,0
                                l115.5-115.5C460.366,409.989,463,403.63,463,397s-2.634-12.989-7.322-17.678l-115.5-115.5c-9.763-9.762-25.593-9.763-35.355,0
                                c-9.763,9.763-9.763,25.592,0,35.355l72.822,72.822H-24c-13.808,0-25,11.193-25,25S-37.808,422-24,422z" />
            </svg>
        </button>
    </div>
@endsection

@section('body-javascript')
    <script>
        (async () => {
            /**
             * Gestion du tableau de données
             */
            // Déclaration de variables
            const table = document.querySelector('#main-table');
            // On va déterminer l'index de la page actuelle
            const urlParams = new URLSearchParams(window.location.search);
            const currentPageIndex = urlParams.get('page') || 1;

            // Call ajax pour récupérer les informations
            const response = await axios.get(
                `http://localhost:8080/api/registre-incidence?page=${currentPageIndex}`);

            // Initialisation de variable en rapport avec les données reçues
            const collection = response.data.collection;
            const onFirstPage = response.data.onFirstPage;
            const hasMorePages = response.data.hasMorePages;

            // Affichage de la collection dans le tableau
            if (!collection.length) {
                // Message si la collection est vide
                let tr = document.createElement('tr');
                tr.classList.add("bg-white", "border-4", "border-gray-200");
                let td = document.createElement("td");
                td.classList.add("text-center");
                td.colSpan = 5;
                td.appendChild(document.createTextNode("Aucun élément"));
                tr.appendChild(td);
                table.tBodies[0].appendChild(tr);
            } else {
                // Message si la collection a au moins un élément. La boucle va ajouter ligne par ligne les éléments
                for (const dataRow of collection) {
                    // Création d'une ligne
                    let tr = document.createElement('tr');
                    tr.classList.add("bg-white", "border-4", "border-gray-200");
                    let td = document.createElement("td");
                    td.appendChild(document.createTextNode(dataRow.id));
                    td.classList.add("px-16", "py-2", "text-center");
                    tr.appendChild(td);

                    td = document.createElement("td");
                    td.appendChild(document.createTextNode(dataRow.jour));
                    td.classList.add("px-16", "py-2", "text-center");
                    tr.appendChild(td);

                    td = document.createElement("td");
                    td.appendChild(document.createTextNode(dataRow.nomReg + " (" + dataRow.numReg +
                        ")"));
                    td.classList.add("px-16", "py-2", "text-center");
                    tr.appendChild(td);

                    td = document.createElement("td");
                    td.appendChild(document.createTextNode(dataRow.incid_rea));
                    td.classList.add("px-16", "py-2", "text-center");
                    tr.appendChild(td);

                    td = document.createElement("td");

                    // Si l'utilisateur est connecté
                    const authTokenTag = document.querySelector('meta[name="auth-token"]');
                    if (authTokenTag.getAttribute('type')=== 'bearer' && authTokenTag.content.length > 0) {
                        // Edit button
                        let button = document.createElement("button");
                        button.classList.add("text-white", "hover:text-indigo-900", "bg-blue-600",
                            "hover:bg-blue-500", "p-2", "mr-2", "rounded-md");
                        button.appendChild(document.createTextNode("Edition"));
                        button.onclick = () => {
                            window.location.href = `${window.location.pathname}/${dataRow.id}/edit`
                        };
                        td.appendChild(button);

                        // Delete button
                        button = document.createElement("button");
                        button.classList.add("text-white", "hover:text-red-900", "bg-red-600", "hover:bg-red-500",
                            "p-2", "mr-2", "rounded-md");
                        button.appendChild(document.createTextNode("Supprimer"));
                        button.onclick = () => {
                            if (confirm('Souhaitez-vous réellement supprimer cette donnée') == true) {
                                entityDelete(dataRow.id)
                            }
                        };
                        td.appendChild(button);
                    } else {
                        td.innerText = "Veuillez vous connecter";
                    }
                    tr.appendChild(td);

                    // Remplissage d'une ligne dans le tableau
                    table.tBodies[0].appendChild(tr);
                }
            }

            /**
             * Gestion des bouttons de pagination
             */
            // Déclaration de variables
            const previousButton = document.querySelector("#previous_button");
            const nextButton = document.querySelector("#next_button");

            // Blocage des boutons si l'on se trouve à la première ou la dernière page
            if (onFirstPage) {
                previousButton.disabled = true;
            }
            if (!hasMorePages) {
                nextButton.disabled = true;
            }

            // Changement de la page au click
            previousButton.addEventListener("click", (event) => {
                document.location.href = `?page=${parseInt(currentPageIndex) - 1}`;
            });
            nextButton.addEventListener("click", (event) => {
                document.location.href = `?page=${parseInt(currentPageIndex) + 1}`;
            });
        })();

        /**
         * Action de suppression d'un item
         */
        function entityDelete(number) {
            // On va récupérer la balise html contenant le token de connexion
            const authTokenTag = document.querySelector('meta[name="auth-token"]');
            const textError = document.querySelector('#error-text');
            const textSuccess = document.querySelector("#success-text");


            if (authTokenTag.getAttribute('type') === 'bearer' || authTokenTag.content.length > 0) {
                axios.delete(`http://localhost:8080/api/registre-incidence/${number}`, {
                        headers: {
                            authorization: `Bearer ${authTokenTag.content}`
                        }
                    })
                    .then(response => {
                        // On cache une possible erreur affichée
                        textError.textContent = "";
                        textError.classList.add("hidden");

                        // On affiche un message de succès
                        textSuccess.classList.remove("hidden");
                        textSuccess.textContent = "L'élément a été supprimé avec succès. Veuillez recharger la page.";
                    })
                    .catch(err => {
                        // On va afficher une erreur (on cache le message de succès au cas où il serait affiché)
                        textError.classList.remove("hidden");
                        textSuccess.classList.add("hidden");

                        if (err.response) {
                            switch (err.response.status) {
                                case 401:
                                    textError.textContent = "Utilisateur non connecté";
                                    break;
                                case 403:
                                    textError.textContent = "Vous n'avez pas les droits nécessaires";
                                    break;
                                case 404:
                                    textError.textContent = "L'élément n'existe pas ou plus";
                                    break;
                                case 500:
                                default:
                                    textError.textContent = "Erreur inconnue";
                                    break;
                            }
                        } else {
                            // Erreur inconnue
                            textError.textContent = "Erreur inconnue";
                        }
                    });
            }

        }

    </script>
@endsection
