@extends('layout')

@section('body')
    <main class="mx-8 mt-16">
        <table class="min-w-full table-auto" id="main-table">
            <thead class="justify-between">
                <tr class="bg-gray-800">
                    <th class="px-16 py-2">
                        <span class="text-gray-300">Id</span>
                    </th>
                    <th class="px-16 py-2">
                        <span class="text-gray-300">Date</span>
                    </th>
                    <th class="px-16 py-2">
                        <span class="text-gray-300">Région</span>
                    </th>
                    <th class="px-16 py-2">
                        <span class="text-gray-300">Nombre d'admis en réanimation</span>
                    </th>
                </tr>
            </thead>
            <tbody class="bg-gray-200">

            </tbody>
        </table>
    </main>
@endsection

@section('body-javascript')
    <script>
        let table = document.querySelector('#main-table');

        axios.get(`http://localhost:8080/api/registre-incidence/{{ $indice }}`).then((response) => {
                if (response.status !== 200) {
                    table.tBodies[0].appendChild(document.createTextNode(
                    "Les données de cette page sont introuvables"));
                    return;
                }
                let dataRow = response.data.data;

                let tr = document.createElement('tr');
                tr.classList.add("bg-white", "border-4", "border-gray-200");
                let td = document.createElement("td");
                td.appendChild(document.createTextNode(dataRow.id));
                td.classList.add("px-16", "py-2", "text-center");
                tr.appendChild(td);

                td = document.createElement("td");
                td.appendChild(document.createTextNode(dataRow.jour));
                td.classList.add("px-16", "py-2", "text-center");
                tr.appendChild(td);

                td = document.createElement("td");
                td.appendChild(document.createTextNode(dataRow.nomReg + " (" + dataRow.numReg + ")"));
                td.classList.add("px-16", "py-2", "text-center");
                tr.appendChild(td);

                td = document.createElement("td");
                td.appendChild(document.createTextNode(dataRow.incid_rea));
                td.classList.add("px-16", "py-2", "text-center");
                tr.appendChild(td);

                table.tBodies[0].appendChild(tr);
            })
            .catch((error) => {
                table.tBodies[0].appendChild(document.createTextNode("Les données de cette page sont introuvables"));
            });

    </script>
@endsection
