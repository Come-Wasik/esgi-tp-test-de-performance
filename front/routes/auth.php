<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register auth routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/auth/register', [RegisteredUserController::class, 'create'])->name('auth.register');

Route::post('/auth/register', [RegisteredUserController::class, 'store'])->name('auth.register.store');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])->name('auth.create');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])->name('auth.store');

Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])->name('auth.destroy');

Route::get('/profil', function () {
    return view('auth.profile');
})->name('auth.profile')
->middleware('has-auth-token');
