<?php

use App\Http\Controllers\RegistreIncidenceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::resource('registre-incidence', RegistreIncidenceController::class)->only(['index', 'create', 'show', 'edit']);
Route::get('registre-incidence-low', [RegistreIncidenceController::class, 'indexLow'])->name('registre-incidence.index-low');