import http from 'k6/http';

import { check, group, sleep, fail } from 'k6';

export let options = {
    vus: 1, // 1 user looping for 1 minute
    duration: '1s',
    thresholds: {
        http_req_duration: ['p(99)<1500'], // 99% of requests must complete below 1.5s
    },
};

// Identifiants de test
const ADMIN_EMAIL = 'taylor@laravel.com';
const MEMBER_EMAIL = 'test@mail.com';
const MAIN_PASSWORD = 'password';

// URLs de l'api
const API_URL = 'http://localhost:8080';
const API_LOGIN_URL = 'api/auth/login';
const API_PROFILE_URL = 'api/auth/profile';
const API_LOGOUT_URL = 'api/auth/logout';
const API_REG_INCID_URL = 'api/registre-incidence';
const API_SELECT_REG_INCID_URL = 'api/registre-incidence/{registre_incidence}';
const API_UPDATE_REG_INCID_URL = 'api/registre-incidence/{registre_incidence}/edit';

export default function () {
    // Variables declaration
    let regIncidId = 0; // An entity id in the database
    let response = null; // Global http responses
    let adminAuthHeaders = {}; // Headers to be loged as an admin
    let memberAuthHeaders = {}; // Headers to be loged as a member

    /**
     * La connexion (admin/membre)
     */
    // Admin login
    response = http.post(`${API_URL}/${API_LOGIN_URL}`, {
        email: ADMIN_EMAIL,
        password: MAIN_PASSWORD,
    });
    // On vérifie que l'utilisateur est bien connecté
    check(response, {
        'Login admin status is 200': (resp) => resp.status === 200,
        'Admin token is available': (resp) => resp.json('token') !== '' && resp.json('token_type') === 'bearer'
    });
    // On prépare les headers pour les futures requêtes
    adminAuthHeaders = {
        headers: {
            Authorization: `Bearer ${response.json('token')}`,
        },
    };

    // Member login
    response = http.post(`${API_URL}/${API_LOGIN_URL}`, {
        email: MEMBER_EMAIL,
        password: MAIN_PASSWORD,
    });
    // On vérifie que l'utilisateur est bien connecté
    check(response, {
        'Login member status is 200': (resp) => resp.status === 200,
        'Member token is available': (resp) => resp.json('token') !== '' && resp.json('token_type') === 'bearer'
    });
    // On prépare les headers pour les futures requêtes
    memberAuthHeaders = {
        headers: {
            Authorization: `Bearer ${response.json('token')}`,
        },
    };

    /**
     * Accès au profil utilisateur
     */
    response = http.get(`${API_URL}/${API_PROFILE_URL}`, adminAuthHeaders);
    check(response, {
        'Autorized to access the profile url': (resp) => resp.status === 200,
        'Can get our email': (resp) => resp.json('data.email') === ADMIN_EMAIL
    });

    /**
     * CRUD sur un élément
     * Dans l'ordre: création, édition, lecture et supression
     */
    // On tente de créer un élément
    response = http.post(`${API_URL}/${API_REG_INCID_URL}`, {
        jour: '2017-11-27',
        nomReg: 'Saint bernard',
        numReg: 17815,
        incid_rea: 18
    }, adminAuthHeaders);
    check(response, {
        'Create reg incid status is 200': (resp) => resp.status === 200,
        'Create reg incid return an id': (resp) => Number.isInteger(resp.json('idModel'))
    });
    // On mémorise l'id de l'élément créé
    regIncidId = response.json('idModel');

    // On met à jour cet élément
    response = http.patch(`${API_URL}/${API_UPDATE_REG_INCID_URL.replace('{registre_incidence}', regIncidId)}`, {
        jour: '2017-11-28',
        nomReg: 'Saint bernardette',
        numReg: 17815,
        incid_rea: 22
    }, adminAuthHeaders);
    check(response, {
        'Update reg incid status is 200': (resp) => resp.status === 200,
        'Updated reg incid return the changed entity': (resp) => resp.json('messages.updatedModel.incid_rea') == 22
    });

    // On visionne cet élément
    response = http.get(`${API_URL}/${API_SELECT_REG_INCID_URL.replace('{registre_incidence}', regIncidId)}`);
    check(response, {
        'View reg incid status is 200': (resp) => resp.status === 200,
        'View reg incid return the current entity': (resp) => resp.json('data.incid_rea') == 22
    });

    // On supprime cet élément
    // Tentative 1 : Un membre n'a pas les droits
    response = http.del(`${API_URL}/${API_SELECT_REG_INCID_URL.replace('{registre_incidence}', regIncidId)}`, null, memberAuthHeaders);
    check(response, {
        'Delete reg incid status is 403 for members': (resp) => resp.status === 403
    });

    // Tentative 2 : Un admin a les droits
    response = http.del(`${API_URL}/${API_SELECT_REG_INCID_URL.replace('{registre_incidence}', regIncidId)}`, null, adminAuthHeaders);
    check(response, {
        'Delete reg incid status is 200 for admins': (resp) => resp.status === 200,
        'Delete reg incid return the current entity': (resp) => resp.json('message') == "The $modelId model does not exists anymore".replace('$modelId', regIncidId)
    });

    /**
     * Déconnexion des deux utilisateurs
     */
    response = http.post(`${API_URL}/${API_LOGOUT_URL}`, null, adminAuthHeaders);
    check(response, {
        'Logout admin status is 200': (resp) => resp.status === 200,
        'Successfully logout message': (resp) => resp.json('message') === 'Successfully logged out'
    });

    response = http.post(`${API_URL}/${API_LOGOUT_URL}`, null, memberAuthHeaders);
    check(response, {
        'Logout member status is 200': (resp) => resp.status === 200,
        'Successfully logout message': (resp) => resp.json('message') === 'Successfully logged out'
    });

    sleep(1);
}