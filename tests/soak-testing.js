import http from 'k6/http';

import { check, group, sleep, fail } from 'k6';

export let options = {
    stages: [
        { duration: '2m', target: 400 }, // ramp up to 400 users
        { duration: '3h56m', target: 400 }, // stay at 400 for ~4 hours
        { duration: '2m', target: 0 }, // scale down. (optional)
    ],
};

// URLs de l'api
const API_URL = 'http://localhost:8080';
const API_SELECT_REG_INCID_URL = 'api/registre-incidence/{registre_incidence}';

export default function () {
    // Variables declaration
    let regIncidId = 1; // An entity id in the database
    let response = null; // Global http responses

    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach(reIncidId => {
        response = http.get(`${API_URL}/${API_SELECT_REG_INCID_URL.replace('{registre_incidence}', regIncidId)}`);
    });

    sleep(1);
}