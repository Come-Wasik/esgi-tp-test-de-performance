import http from 'k6/http';

import { check, group, sleep, fail } from 'k6';

export let options = {
    stages: [
        { duration: '5m', target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
        { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
        { duration: '5m', target: 0 }, // ramp-down to 0 users
    ],
    thresholds: {
        http_req_duration: ['p(99)<1500'], // 99% of requests must complete below 1.5s
        'logged in successfully': ['p(99)<1500'], // 99% of requests must complete below 1.5s
    },
};

// URLs de l'api
const API_URL = 'http://localhost:8080';
const API_SELECT_REG_INCID_URL = 'api/registre-incidence/{registre_incidence}';

export default function () {
    // Variables declaration
    let regIncidId = 1; // An entity id in the database
    let response = null; // Global http responses

    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach(reIncidId => {
        response = http.get(`${API_URL}/${API_SELECT_REG_INCID_URL.replace('{registre_incidence}', regIncidId)}`);
    });

    sleep(1);
}